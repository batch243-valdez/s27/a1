// Users
{
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@mail.com",
    "password": "john1234",
    "isAdmin": false,
    "mobileNo": "09237593671"
},
{
    "id": 2,
    "firstName": "John Bazil",
    "lastName": "Valdez",
    "email": "bazil2314@mail.com",
    "password": "qwerty2314",
    "isAdmin": false,
    "mobileNo": "09253933335"
}

// Orders
{
    "id": 10,
    "userId": 1,
    "productID" : 25,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 300
},
{
    "id": 11,
    "userId": 2,
    "productID" : 18, 25
    "transactionDate": "08-15-2021",
    "status": "Unpaid",
    "total": 950
}

// Products
{
    "id": 25,
    "name": "Humidifier",
    "description": "Make your home smell fresh any time.",
    "price": 300,
    "stocks": 1286,
    "isActive": true,
},
{
    "id": 18,
    "name": "Iron",
    "description": "Lets make your clothes more presentable!",
    "price": 650,
    "stocks": 111830,
    "isActive": true,
}

